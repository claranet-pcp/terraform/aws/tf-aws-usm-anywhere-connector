# tf-aws-usm-anywhere-connector
USM Anywhere Cloud Connector deployment. Adapted from the CloudFormation template and [documentation](https://cybersecurity.att.com/documentation/usm-anywhere/deployment-guide/cloud-connector/aws/uploading-cloud-formation-template.htm).

Additionally creates the S3 notification rules on the buckets provided.

## Usage
```
module "connector" {
  source = "/path/to/module"

  name           = "attcs-s3-connector-xxx"
  aws_account_id = "123456789123"
  external_id    = "abc-xyz"
  s3_bucket_ids  = ["my-log-bucket"]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
#### Requirements

No requirements.

#### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

#### Modules

No modules.

#### Resources

| Name | Type |
|------|------|
| [aws_iam_policy.connector](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.connector](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.connector](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_s3_bucket_notification.connector](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification) | resource |
| [aws_sns_topic.connector](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |
| [aws_sns_topic_policy.connector](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_policy) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.connector_assume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.connector_s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.connector_sns](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

#### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_account_id"></a> [aws\_account\_id](#input\_aws\_account\_id) | AWS account ID provided by AlienVault | `string` | n/a | yes |
| <a name="input_enable_s3_bucket_notifications"></a> [enable\_s3\_bucket\_notifications](#input\_enable\_s3\_bucket\_notifications) | Toggle S3 bucket notifications to the AlienVault SNS topic | `bool` | `true` | no |
| <a name="input_external_id"></a> [external\_id](#input\_external\_id) | External ID provided by AlienVault | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Name of resources provided by AlienVault | `string` | n/a | yes |
| <a name="input_s3_bucket_ids"></a> [s3\_bucket\_ids](#input\_s3\_bucket\_ids) | S3 bucket ID's to grant the connector access to | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags to add to resources | `map(string)` | `{}` | no |

#### Outputs

| Name | Description |
|------|-------------|
| <a name="output_sns_topic_arn"></a> [sns\_topic\_arn](#output\_sns\_topic\_arn) | AlienVault SNS topic ARN |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
