output "sns_topic_arn" {
  description = "AlienVault SNS topic ARN"
  value       = aws_sns_topic.connector.arn
}
