locals {
  s3_bucket_arns = formatlist("arn:aws:s3:::%s", var.s3_bucket_ids)
  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )
}

resource "aws_iam_role" "connector" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.connector_assume.json

  tags = local.tags
}

data "aws_iam_policy_document" "connector_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.aws_account_id}:root"]
    }

    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"
      values   = [var.external_id]
    }
  }
}

resource "aws_iam_policy" "connector" {
  name   = var.name
  policy = data.aws_iam_policy_document.connector_s3.json
}

data "aws_iam_policy_document" "connector_s3" {
  statement {
    actions = ["s3:ListBucket"]

    resources = local.s3_bucket_arns
  }

  statement {
    actions = ["s3:GetObject"]

    resources = formatlist("%s/*", local.s3_bucket_arns)
  }
}

resource "aws_iam_role_policy_attachment" "connector" {
  policy_arn = aws_iam_policy.connector.arn
  role       = aws_iam_role.connector.name
}

resource "aws_sns_topic" "connector" {
  name = var.name

  tags = local.tags
}

resource "aws_sns_topic_policy" "connector" {
  arn = aws_sns_topic.connector.arn

  policy = data.aws_iam_policy_document.connector_sns.json
}

data "aws_iam_policy_document" "connector_sns" {
  policy_id = var.name

  statement {
    sid = "S3Publish"

    actions = ["sns:Publish"]

    principals {
      type        = "Service"
      identifiers = ["s3.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "aws:SourceAccount"
      values   = [data.aws_caller_identity.current.account_id]
    }

    condition {
      test     = "ArnLike"
      variable = "aws:SourceArn"
      values   = local.s3_bucket_arns
    }

    resources = [aws_sns_topic.connector.arn]
  }

  statement {
    sid = "SNSSubscribe"

    actions = ["sns:Subscribe"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.aws_account_id}:root"]
    }

    resources = [aws_sns_topic.connector.arn]
  }

  statement {
    sid = "__default_statement_ID"

    actions = [
      "sns:Subscribe",
      "sns:SetTopicAttributes",
      "sns:RemovePermission",
      "sns:Receive",
      "sns:Publish",
      "sns:ListSubscriptionsByTopic",
      "sns:GetTopicAttributes",
      "sns:DeleteTopic",
      "sns:AddPermission",
    ]

    condition {
      test     = "StringEquals"
      variable = "aws:SourceOwner"
      values   = [data.aws_caller_identity.current.account_id]
    }

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [aws_sns_topic.connector.arn]
  }
}

resource "aws_s3_bucket_notification" "connector" {
  for_each = var.enable_s3_bucket_notifications ? toset(var.s3_bucket_ids) : []

  bucket = each.value

  topic {
    id        = var.name
    topic_arn = aws_sns_topic.connector.arn
    events    = ["s3:ObjectCreated:*"]
  }
}
