variable "name" {
  type        = string
  description = "Name of resources provided by AlienVault"
}

variable "aws_account_id" {
  type        = string
  description = "AWS account ID provided by AlienVault"
}

variable "external_id" {
  type        = string
  description = "External ID provided by AlienVault"
}

variable "s3_bucket_ids" {
  type        = list(string)
  description = "S3 bucket ID's to grant the connector access to"
}

variable "enable_s3_bucket_notifications" {
  type        = bool
  description = "Toggle S3 bucket notifications to the AlienVault SNS topic"
  default     = true
}

variable "tags" {
  type        = map(string)
  description = "Map of tags to add to resources"
  default     = {}
}
